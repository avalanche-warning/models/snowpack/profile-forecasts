#! /usr/bin/python3
from datetime import datetime, timedelta, date
import time
import glob
import shutil
import os
import sys
import subprocess
import configparser
import warnings
import json
import multiprocessing as mp
warnings.simplefilter(action='ignore', category=FutureWarning)

import pandas as pd

from awsmet.smet_parser import SMETParser
import awio
from awio.filetools import get_scriptpath
from awset import get, exists, getlist, getattribute, DEFAULT_DOMAIN_PATH, awsome_env
from snowpacktools.caaml import caamlv6_processor
from snowpacktools.snowpro import snowpro
from snowpacktools.avapro import avapro

import nwp_provider as awnwp
import profile_provider as awprof_provider
from profsnow.snowpack.SNEngine import SNEngine
from qmah.processing import process_vstations as pv

date_format       = '%Y-%m-%d'
datetime_format   = '%Y-%m-%dT%H:%M'
datetime_format_2 = '%Y-%m-%dT%H:%M:%S'
debug_mode_dict = {
    '0': 'run all',
    '1': 'only postprocessing',
    '2': 'skip NWP update and SMET file generation and start simulating as quickly as possible',
    }

_upload_script_path = os.path.expandvars('$AWSOME_BASE/visualization/upload/upload.py')

def run_domain(domain="default"):
    """Running snowobs-chain 
    
    Workflow:
    1. Fetch snow observations (snow profiles) and prepare the CAAML files for SNOWPACK initialization
    2. Prepare weather data by stacking NWP model data
    3. Generate SMET files with relevant weather data for each location with a snow observation
    4. Run snowpack on all locations
    5. Postprocess and visualize results with AVAPRO
    6. Upload results to server
    """

    print(f"[i]  Starting snowobs-chain at {datetime.now()}.")

    cfg, _domain_path = _read_config(domain)
    cfg = _get_date_opera(cfg)
    
    ## - Obtain observed snow profiles - ##
    if getattribute(['snowobs-chain', "profile", "download_profiles"], "enabled", domain, cfgpath=_domain_path) == "true":
        print('[i]  Downloading snow profiles.')
        awprof_provider.get_and_prepare_profiles_for_snp(domain, "snowobs-chain", _domain_path)
    else:
        print('[i]  Snow profiles not downloaded again (download_profiles set to -false-).')
    
    vstation_csv_path = os.path.join(cfg.get('Paths', '_vstations_csv_dir'), f"vstations-{domain}.csv")
    df_vstations = generate_vstations_df(cfg.get('Paths','_profile_dl_dir'), vstation_csv_path) 

    ## - Weather data: Generating NETCDF files (and SMET files) - ##
    ncpus = int(get(["snowobs-chain", "run", "ncpus"], domain, cfgpath=cfg.get('Paths', '_domain_path'), throw=True))
    if cfg.get('General','debug_mode') in ["1","2"]:
        print("[i]  Skipping NWP update and skipping SMET file generation.")
        sys.stdout.flush()
        ds_nwp  = awnwp.open_dataset(domain, 'snowobs-chain', 'nowcast', update_dataset=False, aux_config=cfg)
        end_nowcast = datetime.strftime(pd.Timestamp(ds_nwp.time[-1].values), format=datetime_format)
        ds_nwp.close()
        if cfg.getboolean('General', 'run_forecast'):
            ds_nwp_fc  = awnwp.open_dataset(domain, 'snowobs-chain', 'forecast', update_dataset=False, aux_config=cfg)
            end_forecast = datetime.strftime(min(pd.Timestamp(ds_nwp_fc.time[-1].values), datetime.strptime(cfg.get('General', 'season_end'), date_format)), datetime_format)
            ds_nwp_fc.close()
    else:
        print("[i]  Preparing weather data.")
        sys.stdout.flush()
        shutil.rmtree(cfg.get('Paths','_smet_files_dir_nowcast'), ignore_errors=True)
        shutil.rmtree(cfg.get('Paths','_smet_files_dir_forecast'), ignore_errors=True)

        ds_nwp  = awnwp.open_dataset(domain, 'snowobs-chain', 'nowcast', update_dataset=True, aux_config=cfg, prefer_nc_over_db=True)
        meteo_vars = get(cfg.get('Paths', f'_xml_path_meteo_nowcast').split('/') + ['variables'], domain, cfg.get('Paths', '_domain_path'))
        print("[i]  Generating SMET files for nowcast.")
        awnwp.smet_generator.generate_smets_for_pois(ds_nwp, df_vstations, cfg.get('Paths','_smet_files_dir_nowcast'),
                                                meteo_vars, ncpus=ncpus)
        end_nowcast = datetime.strftime(pd.Timestamp(ds_nwp.time[-1].values), format=datetime_format)
        ds_nwp.close()
        if cfg.getboolean('General', 'run_forecast'):
            ds_nwp_fc  = awnwp.open_dataset(domain, 'snowobs-chain', 'forecast', update_dataset=True, aux_config=cfg)
            meteo_vars = get(cfg.get('Paths', f'_xml_path_meteo_forecast').split('/') + ['variables'], domain, cfg.get('Paths', '_domain_path'))
            print("[i]  Generating SMET files for forecast.")
            awnwp.smet_generator.generate_smets_for_pois(ds_nwp_fc, df_vstations, cfg.get('Paths','_smet_files_dir_forecast'),
                                                    meteo_vars, ncpus=ncpus)
            end_forecast = datetime.strftime(min(pd.Timestamp(ds_nwp_fc.time[-1].values), datetime.strptime(cfg.get('General', 'season_end'), date_format)), datetime_format)
            ds_nwp_fc.close()

    if cfg.getboolean('General', 'resume') and (len(glob.glob(os.path.join(cfg.get('Paths','_sno_nowcast_dir'), "*.sno"))) > 0):
        begin_nowcast = SMETParser.get_header(sorted(glob.glob(os.path.join(cfg.get('Paths','_sno_nowcast_dir'), "*.sno")))[0])["ProfileDate"]
        print(f"[i]  Resuming from .sno-files activated. Starting with profiles from {begin_nowcast}.")
    else:
        print(f"[i]  Resuming from .sno-files NOT activated. Starting from original CAAML files.")

    ## - SNOWPACK simulations - ##
    if cfg.get('General','debug_mode') != "1":
        SNEngine.run_awsome_snowpack(domain,"snowobs-chain")        
    else:
        print("[i]  Skipping snow cover simulations. Continue with postprocessing.")

    ## - AVAPRO - ##
    if getattribute(['snowobs-chain', 'output', 'avapro'], "enabled", domain, cfgpath=cfg.get('Paths','_domain_path')) == "true":
        # shutil.rmtree(config.get('Paths','_avapro_figs_output_dir'), ignore_errors=True)
        # shutil.rmtree(config.get('Paths','_avapro_output_dir'), ignore_errors=True)
        os.makedirs(cfg['Paths']['_avapro_output_dir'], exist_ok=True)
        os.makedirs(cfg['Paths']['_avapro_figs_output_dir'], exist_ok=True)

        cfg_avapro = _prepare_avapro_ini(cfg)
        stime = time.time()
        
        progress_counter = mp.Manager().Value('i', 0)
        lock = mp.Manager().Lock()
        stime = time.time()
        pbar = {"progress_counter": progress_counter, "lock": lock, "stime": stime, "ntasks": len(df_vstations)}

        running_procs = []
        sema  = mp.Semaphore(cfg.getint("General","ncpus"))
        for ii, row in df_vstations.iterrows():
            vstation = row.to_dict()
            for p in running_procs[:]:
                if not p.is_alive():
                    p.join()
                    running_procs.remove(p)
            sema.acquire()
            proc = mp.Process(target=_call_avapro, args=(ii, vstation, cfg_avapro, pbar, sema))
            running_procs.append(proc)
            proc.start()    
        for proc in running_procs:
            proc.join()

        print("")
        print(f"[i]  Characterizing avalanche problems with AVAPRO completed in {awio.datetools.iso_timediff(stime)} hours.")

        # - Initialize profile json file - #
        with open(cfg['Paths']['observed_profiles_json'], "w") as filename:
            filename.write('[')

        for ii, row in df_vstations.iterrows():
            vstation = row.to_dict()
        
            # Preparing JSON metadata
            external_url = os.path.join(get(['snowobs-chain', "output", "upload", "link"], domain, cfgpath=cfg.get('Paths','_domain_path')), vstation["filename"] + ".png")
            internal_url = os.path.join(cfg.get("Paths","_avapro_figs_output_dir"), vstation["filename"] + ".png")
            generic_observation_dict = {"latitude"      : vstation['lat'],
                                        "longitude"     : vstation['lon'],
                                        "elevation"     : vstation['elev'],
                                        "aspect"        : vstation['aspect'],
                                        "eventDate"     : vstation['datetime'],
                                        "locationName"  : vstation['name'],
                                        "$internalURL"  : internal_url,
                                        "$externalURL"  : external_url}

            with open(cfg['Paths']['observed_profiles_json'], "a") as filename:
                if ii != 0:
                    filename.write(',')
                json.dump(generic_observation_dict, filename)
        
        # - Finish profile json file - #
        with open(cfg['Paths']['observed_profiles_json'], "a") as filename:
            filename.write(']')
        
        _upload_files(cfg,domain,cfg['Paths']['observed_profiles_json'],cfg['Paths']['_avapro_figs_output_dir'])

    ini_files = awio.multiglob("./input/snp_io*.ini", "./input/avapro*.ini")
    for file in ini_files:
        os.remove(file)

    if getattribute(["snowobs-chain", "output", "qmah-geojson"], "enabled", domain, cfgpath=cfg.get('Paths','_domain_path')) == "true":
        cfg.read(cfg.get("Paths","_ini_runtime_domain"))
        ## - Identify timestamps for generating geojson files (e.g. for last week and forecast) - ##  
        ts_start = datetime.strptime(cfg.get('General','date_opera'), date_format) - timedelta(days=5)
        # ts_start = datetime.strptime(config.get('Gridded-chain','begin_nowcast'), datetime_format)
        if cfg.getboolean('General','run_forecast'):
            ts_end = datetime.strptime(end_forecast, datetime_format)
        else:
            ts_end = datetime.strptime(end_nowcast, datetime_format)
        
        if cfg.getboolean("General","resume") == False:
            shutil.rmtree(cfg.get('Paths','_geojson_dir'), ignore_errors=True)
        os.makedirs(cfg.get('Paths','_geojson_dir'), exist_ok=True)
        _call_qmah_and_upload_geojson(domain, cfg, ts_start, ts_end)


    ## -Generate Map - ##
    # map_path = os.path.join(get(['snowobs-chain', "output", "upload", "host"], domain, cfgpath=cfg.get('Paths','_domain_path')), "profile_forecast_map.html")
    # map_link = os.path.join(get(['snowobs-chain', "output", "upload", "link"], domain, cfgpath=cfg.get('Paths','_domain_path')), "profile_forecast_map.html")
    # map_data = {       
    #     "map_path":          map_path, 
    #     "microregions":      cfg.get('Paths','_microregions_dir'),
    #     "observed_profiles": cfg['Paths']['observed_profiles_json'],
    #     "dem_from_nwp":      get(["domain", "shape", "file"], domain, cfgpath=_domain_path)
    # }
    # returnCode = subprocess.call(["python3", cfg.get('Paths','leaflet_map_script_path'), "--map_path", map_data["map_path"], 
    #                               "--microregions", map_data["microregions"], "--observed_profiles", map_data['observed_profiles'], "--dem_from_nwp", map_data['dem_from_nwp']])
    # if returnCode==0:  print("[i]  Leaflet map access: {}".format(map_link))
    # else:
    #     print("[E]  Generating leaflet map not successful!")


def _call_qmah_and_upload_geojson(domain: str, config: object, ts_start: datetime, ts_end: datetime):

    print("[i]  Generating GEOJSON files for dashboards with QMAH...")
    stime = time.time()
    
    # call qmah.processing.process_vstations:
    # TODO: customize stability indices based on domain.xml    
    # TODO: integrate datetag feature based on domain.xml
    vstation_csv_path = os.path.join(config.get('Paths', '_vstations_csv_dir'), f"vstations-{domain}.csv") 
    pv.process_vstations(config.get('General', 'domain'), vstation_csv_path, config.get('Paths', '_geojson_dir'), 
                      ts_start, ts_end, dt=timedelta(hours=12), aspects=None,
                      ncpus=config.getint('General', 'ncpus'))

    print(f"[i]  Writing GEOJSON files completed in {awio.iso_timediff(stime)} hours.")
    if exists(["snowobs-chain", "output", "upload"], domain, cfgpath=config.get('Paths','_domain_path')):
        files = glob.glob(f"{config['Paths']['_geojson_dir']}/*.json")
        host  = get(["snowobs-chain", "output", "upload", "host"], domain, cfgpath=config.get('Paths','_domain_path'))
        link  = get(["snowobs-chain", "output", "upload", "link"], domain, cfgpath=config.get('Paths','_domain_path'))
        host = os.path.join(host, f"geojson-{domain}")
        link = link + f"/geojson-{domain}"

        os.makedirs(host, exist_ok=True)
        print(f"[i]  Uploading folder: {config['Paths']['_geojson_dir']} to {host} ...")
        for file in files:
            subprocess.call(["python3", _upload_script_path, file, host])
        print(f"[i]  Uploaded files are available at: {link}")



def _read_config(domain):
    """Define relevant parameters from domain and folders"""
    cfg = configparser.ConfigParser()
    cfg.optionxform = str
    hpath           = "~"
    snowobs_chain_dir = os.path.expanduser("~snowpack/snowobs-chain")
    spath           = get_scriptpath(__file__)[:-1] # remove last /
    _domain_path       = DEFAULT_DOMAIN_PATH

    os.makedirs(f"{snowobs_chain_dir}/data", exist_ok=True)
    os.makedirs(f"{snowobs_chain_dir}/input", exist_ok=True)
    os.makedirs(f"{snowobs_chain_dir}/output", exist_ok=True)

    if getattribute(["snowobs-chain", "output", "directory"], "enabled", domain, cfgpath=_domain_path) == "true":
        _output_dir = get(["snowobs-chain", "output", "directory"], domain, cfgpath=_domain_path, throw=True)
    else:
        _output_dir = os.path.expanduser("~snowpack/gridded-chain/output")
    if getattribute(["snowobs-chain", "output", "snp-directory"], "enabled", domain, cfgpath=_domain_path) == "true":
        _snp_output_dir = get(["snowobs-chain", "output", "snp-directory"], domain, cfgpath=_domain_path, throw=True)
    else:
        _snp_output_dir = f'{_output_dir}/snp-{domain}'

    cfg['Paths'] = {'_domain_path' :                    _domain_path,
                    '_ini_runtime_domain':            os.path.join(f"{snowobs_chain_dir}/input", "runtime_" + domain + ".ini"),
                    '_profile_dl_dir':               f'{spath}/data/profiles/{domain}',
                    '_smet_files_dir_nowcast':       f'{snowobs_chain_dir}/data/smet-files/{domain}/nowcast',
                    '_smet_files_dir_forecast':      f'{snowobs_chain_dir}/data/smet-files/{domain}/forecast',
                    '_sno_nowcast_dir':              f'{snowobs_chain_dir}/data/snow-nowcast/{domain}',
                    '_vstations_csv_dir':            f'{snowobs_chain_dir}/data/vstations',
                    '_output_dir':                   f'{_output_dir}',
                    '_snp_output_dir':               f'{_snp_output_dir}',
                    '_geojson_dir':                  f'{spath}/output/geojson-{domain}',
                    '_avapro_output_dir':            f'{spath}/output/avapro-{domain}',
                    '_avapro_figs_output_dir':       f'{spath}/output/avapro-figures-{domain}',
                    '_snowpro_figs_output_dir':      f'{spath}/output/snowpro-figures-{domain}',
                    'observed_profiles_json':        f'{spath}/output/observed_profiles.json',
                    '_microregions_dir':             get(["domain", "shape", "file"], domain, cfgpath=_domain_path),
                    '_snp_ini_file':                 get(['snowobs-chain', 'snowpack', 'snp_ini_file'], domain, cfgpath=_domain_path, throw=True) \
                                                        if getattribute(['snowobs-chain', 'snowpack', 'snp_ini_file'], 'enabled', domain, cfgpath=_domain_path) == "true" \
                                                        else 'default'}
                    

    cfg['General'] = {'ncpus':                      "1",
                      'domain':                     domain,
                      'season_start':               get(['snowobs-chain', "run", "season_start"], domain, cfgpath=_domain_path),             
                      'season_end':                 get(['snowobs-chain', "run", "season_end"], domain, cfgpath=_domain_path),                  
                      'date_opera':                 get(['snowobs-chain', "run", "date_opera"], domain, cfgpath=_domain_path),
                      'debug_mode':                 get(['snowobs-chain', 'run', 'debug_mode'], domain, cfgpath=_domain_path, throw=True) \
                                                        if getattribute(['snowobs-chain', 'run', 'debug_mode'], 'enabled', domain, cfgpath=_domain_path) == "true" \
                                                        else "0"}
    if getattribute(["snowobs-chain", "run", "ncpus"], "enabled", domain, cfgpath=_domain_path) == "true":
        cfg['General']['ncpus']  = get(['snowobs-chain', "run", "ncpus"], domain, cfgpath=_domain_path)

    """Simulation modes"""
    if getattribute(['snowobs-chain', "run", "resume"], "enabled", domain, cfgpath=_domain_path) == "true":
        cfg['General']['resume']  = "TRUE"
    else:
        cfg['General']['resume']  = "FALSE"
    if exists(['snowobs-chain', "meteo", "source"], domain, _domain_path):
        cfg['Paths']['_xml_path_meteo_nowcast'] = 'snowobs-chain/meteo'
        cfg['Paths']['_xml_path_meteo_forecast'] = 'snowobs-chain/meteo'
        cfg['General']['run_forecast'] = "TRUE"
    elif exists(['snowobs-chain', "meteo", "nowcast", "source"], domain, _domain_path):
        cfg['Paths']['_xml_path_meteo_nowcast'] = 'snowobs-chain/meteo/nowcast'
        if getattribute(['snowobs-chain', 'meteo', 'forecast'], 'enabled', domain, cfgpath=_domain_path) == "true":
            cfg['General']['run_forecast'] = "TRUE"
            cfg['Paths']['_xml_path_meteo_forecast'] = 'snowobs-chain/meteo/forecast'
        else:
            cfg['General']['run_forecast'] = "FALSE"

    """Expand absolute (user) paths"""
    cfg = awio.expanduser(cfg)

    return cfg, _domain_path


def _get_date_opera(cfg):
    """Flexible operational date for virtual operation"""

    if cfg.get('General','date_opera') == 'TODAY':
        date_opera = date.today()
    else:
        date_opera = datetime.strptime(cfg.get('General','date_opera'), date_format).date()
    date_season_end = datetime.strptime(cfg.get('General','season_end'), date_format).date()
    if date_opera <= date_season_end:
        cfg['General']['date_opera'] = datetime.strftime(date_opera, date_format)
    else:
        cfg['General']['date_opera'] = datetime.strftime(date_season_end, date_format)
    return cfg


def _call_avapro(ii, vstation, cfg_avapro, pbar=None, sema=None):
    """Calling AVAPRO in parallel: Generating ini and running AVAPRO"""

    cfg_avapro['AVAPRO']['SNP_FILE'] = vstation['filename'] + ".pro"
    _avapro_ini_file = "./input/avapro" + str(ii) + ".ini"
    with open(_avapro_ini_file, 'w') as cfgfile:
        cfg_avapro.write(cfgfile)

    avapro.avapro(_avapro_ini_file)
    os.remove(_avapro_ini_file)
    sema.release()

    if pbar is not None:
        awio.model_chain_helpers.progress_bar(pbar['progress_counter'], pbar['lock'], pbar["stime"], pbar['ntasks'])
    if sema is not None:
        sema.release()


def _upload_files(config: object, domain: str,observed_profiles_json: str, source_dir: str):
    """Upload all files of sourece directory to webserver / folder"""
    files = glob.glob(f"{source_dir}/*.png")

    if exists(['snowobs-chain', "output", "upload"], domain, cfgpath=config.get('Paths','_domain_path')):
        host = get(['snowobs-chain', "output", "upload", "host"], domain, cfgpath=config.get('Paths','_domain_path'))
        # host = "./output/upload-test" ## TEST
        # os.makedirs(host, exist_ok=True) ## TEST
        for file in files:
            print(f'[i]  Uploading "{file}"...')
            subprocess.call(["python3", _upload_script_path, file, host])
        subprocess.call(["python3", _upload_script_path, observed_profiles_json, host])


def generate_vstations_df(caaml_dir: str, csv_path: str):
    print("[i]  Generate vstations df and csv file.")

    # columns     = ["vstation", "easting", "northing", "lon", "lat", "elev"]
    vstations = []
    prof_files = sorted(glob.glob(os.path.join(caaml_dir,"*.caaml")))
    for prof in prof_files:
        prof_meta = caamlv6_processor.get_prof_metadata(prof)
        prof_meta['filename'] = prof.split("/")[-1].split(".")[0]
        prof_meta['vstation'] = prof_meta['filename']
        prof_meta['elev']     = prof_meta.pop('alt')
        prof_meta['date']     = prof_meta['datetime'][:10]
        prof_meta['time']     = prof_meta['datetime'][11:16].replace(':','h')
        prof_meta['_date']    = prof_meta['date'].replace('-', '')
        prof_meta['name']     = prof_meta['name'].replace(' ','-')
        prof_meta['name']     = prof_meta['name'].replace('/','-')
        longname = prof_meta['date'] + 'T' + prof_meta['time'] + '-' + prof_meta['name'] + '-' + str(int(prof_meta['elev'])) + 'm'
        try:
            longname = longname + '-' + prof_meta['aspect']
        except:
            longname = longname + '-flat'
            prof_meta['aspect'] = ""
        prof_meta['longname'] = longname

        vstations.append(prof_meta)

    df_vstations = pd.DataFrame(vstations)
    os.makedirs(os.path.dirname(csv_path), exist_ok=True)
    df_vstations.to_csv(csv_path, index=False)
    print(f"[i]  File {csv_path} created with {df_vstations.shape[0]} virtual stations.")
    return df_vstations


def _prepare_snowpro_ini(cfg: object, prof_meta: dict):
    """Update paths (and eventually settings) of Snowpro ini file."""

    configs_snowpro = configparser.ConfigParser()
    configs_snowpro.read(cfg.get('Paths','_snowpro_ini_template_path'))
    
    configs_snowpro['SNOWPRO']['PRO_FILE_PATH']  = os.path.join(cfg['Paths']['_snp_output_dir'], prof_meta['filename'] + ".pro")
    configs_snowpro['SNOWPRO']['OUTPUT_DIR']     = cfg['Paths']['_snowpro_figs_output_dir']
    configs_snowpro['SNOWPRO']['OUTPUT_NAME']    = prof_meta['filename'] + ".png"

    # Date of plotted profile
    date_today = date.today()
    datetime_format = '%Y-%m-%d'
    configs_snowpro['SNOWPRO-PROF']['DATETIME'] = datetime.strftime(date_today, datetime_format) + 'T06h00'

    with open(cfg.get('Paths','_snowpro_ini_path'), 'w') as configfile:
        configs_snowpro.write(configfile)

def _prepare_avapro_ini(cfg: object):
    """Update paths (and eventually settings) of avapro ini file."""

    config_avapro = configparser.ConfigParser()
    config_avapro.read(cfg.get('Paths','_avapro_ini_template_path'))

    config_avapro['AVAPRO']['SNP_DIR']         = cfg['Paths']['_snp_output_dir']
    config_avapro['AVAPRO']['OUTPUT_DIR']      = cfg['Paths']['_avapro_output_dir']
    config_avapro['AVAPRO']['OUTPUT_DIR_FIGS'] = cfg['Paths']['_avapro_figs_output_dir']
    config_avapro['AVAPRO']['date_opera']      = cfg.get('General','date_opera')
    config_avapro['AVAPRO']['season_start']    = cfg.get('General','season_start')
    config_avapro['AVAPRO']['season_end']      = cfg.get('General','season_end')

    return config_avapro


if __name__ == "__main__":
    if len(sys.argv) > 2:
        sys.exit("[E] Synopsis: python3 run_profile_forecasts.py [domain]")
    domain = "default"
    if len(sys.argv) >= 2:
        domain = sys.argv[1]
    run_domain(domain)